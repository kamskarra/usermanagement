package com.example.usermanagement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.usermanagement.exception.UserNotFoundException;
import com.example.usermanagement.model.User;
import com.example.usermanagement.repository.AuditRepository;
import com.example.usermanagement.repository.UserRepository;

@SpringBootTest
public class UserServiceTest {
	
	@InjectMocks
	UserService userservice;
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	AuditRepository auditRepository;
	
	@Test
	public void createUserTest() {
		
		User user = new User();
		user.setActive(true);
		user.setCreatedAt(LocalDateTime.now());
		user.setEmail("test@gmail.com");
		user.setFirstName("Test");
		user.setLastName("T");
		user.setRoleName("TEST");
		
		userservice.createUser(user, 1);
		
		verify(userRepository, times(1)).save(user);
		
	}
	
	@Test
	public void getUserByIdTest() throws UserNotFoundException
	{
		User user = new User();
		user.setActive(true);
		user.setCreatedAt(LocalDateTime.now());
		user.setEmail("test@gmail.com");
		user.setFirstName("Test");
		user.setLastName("T");
		user.setRoleName("TEST");
		user.setId(1);
		
		when(userRepository.findById(1)).thenReturn(Optional.of(user));
		
		User result = userservice.getUserById(1);
		
		assertEquals(1, result.getId());
		assertEquals("Test", result.getFirstName());
	}
	
	@Test
	public void getBookByIdTestNegative() {

		try {
			userservice.getUserById(1);
			fail("Expected an UserNotFoundException to be thrown");
		} catch (UserNotFoundException e) {
			System.out.println(e.getMessage());
			assertEquals("No user with id 1", e.getMessage());
		}

	}

	@Test
	public void getAllBooksTest() {
		User user = new User();
		user.setActive(true);
		user.setCreatedAt(LocalDateTime.now());
		user.setEmail("test@gmail.com");
		user.setFirstName("Test");
		user.setLastName("T");
		user.setRoleName("TEST");
		user.setId(1);

		List<User> list = new ArrayList<>();
		list.add(user);

		when(userRepository.findAll()).thenReturn(list);

		List<User> result = (List<User>) userservice.getAllUsers();

		assertEquals(1, result.size());

	}
	
	@Test
	public void updateUserTest() throws UserNotFoundException {
		
		User user = new User();
		user.setActive(true);
		user.setCreatedAt(LocalDateTime.now());
		user.setEmail("test@gmail.com");
		user.setFirstName("Test");
		user.setLastName("T");
		user.setRoleName("TEST");
		user.setId(1);
		
		when(userRepository.findById(1)).thenReturn(Optional.of(user));
		
		userservice.updateUser(user, 1);
		
		verify(userRepository, times(1)).save(user);
		
	}
	
	@Test
	public void deleteUserTest() throws UserNotFoundException {
		
		User user = new User();
		user.setActive(true);
		user.setCreatedAt(LocalDateTime.now());
		user.setEmail("test@gmail.com");
		user.setFirstName("Test");
		user.setLastName("T");
		user.setRoleName("TEST");
		user.setId(1);
		
		when(userRepository.findById(1)).thenReturn(Optional.of(user));
		
		userservice.deleteUser(1, 1);
		
		verify(userRepository, times(1)).deleteById(1);
		
	}


}

package com.example.usermanagement.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.usermanagement.exception.UserNotFoundException;
import com.example.usermanagement.model.Audit;
import com.example.usermanagement.model.User;
import com.example.usermanagement.repository.AuditRepository;
import com.example.usermanagement.repository.UserRepository;


@Service
public class UserService {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AuditRepository auditRepository;
	
	public void createUser(User user, int loggedUserId) {
		userRepository.save(user);
		
		createAudit(user.getId(), loggedUserId, "POST");
		
	}
	
	public void createAudit(int userId, int loggedUserId, String action) {
		Audit audit = new Audit();
		audit.setUserId(userId);
		audit.setLoggedUserId(loggedUserId);
		audit.setCreatedAt(LocalDateTime.now());
		audit.setAction(action);
		auditRepository.save(audit);
	}
	
	public Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}
	
	public User getUserById(int id) throws UserNotFoundException {
		
		Optional<User> user = userRepository.findById(id);

		if (user.isPresent()) {
			return user.get();
		} else {
			LOGGER.error("Error - No user with id " + id);
			throw new UserNotFoundException("No user with id " + id);
		}
	}
	
	public void updateUser(User user, int loggedUserId) throws UserNotFoundException {
		Optional<User> userRec = userRepository.findById(user.getId());
		
		if (userRec.isPresent()) {
			userRepository.save(user);
			createAudit(user.getId(), loggedUserId, "PUT");
		} else {
			LOGGER.error("No user with id " + user.getId());
			throw new UserNotFoundException("No user with id " + user.getId());
		}
	}
	
	public void deleteUser(int id, int loggedUserId) throws UserNotFoundException {
		
		Optional<User> user = userRepository.findById(id);

		if (user.isPresent()) {
			userRepository.deleteById(id);
			createAudit(id, loggedUserId, "DELETE");
		} else {
			LOGGER.error("No user with id " + id);
			throw new UserNotFoundException("No user with id " + id);
		}
	}
	
	public List<Audit> getAuditRecords(String action, Optional<LocalDate> fromDate, Optional<LocalDate> toDate) {
		return auditRepository.getAuditRecords(action, fromDate, toDate);
	}
}

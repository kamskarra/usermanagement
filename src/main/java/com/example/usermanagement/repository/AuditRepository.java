package com.example.usermanagement.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.usermanagement.model.Audit;

public interface AuditRepository extends JpaRepository<Audit, Integer>{
	
	// Native query
		@Query(value = "SELECT * FROM AUDIT WHERE ACTION = ?1 AND created_at BETWEEN ?2 AND ?3", nativeQuery = true)
		List<Audit> getAuditRecords(String action, Optional<LocalDate> fromDate, Optional<LocalDate> toDate);

}

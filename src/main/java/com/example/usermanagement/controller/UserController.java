package com.example.usermanagement.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermanagement.exception.UserNotFoundException;
import com.example.usermanagement.model.Audit;
import com.example.usermanagement.model.User;
import com.example.usermanagement.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller to perform CRUD operations on User
 * 
 * @author User1
 *
 */
@RestController
@Api(description = "CRUD operations on User")
public class UserController {

	@Autowired
	UserService userService;

	private static final int LOGGED_USER_ID = 1;

	private static final String LOGGED_USER_ROLE = "ADMIN";

	/**
	 * 
	 * @param user userObj
	 * @return user id
	 * @throws Exception
	 */
	@PostMapping("/user")
	@ApiOperation(value = "Create User")
	private ResponseEntity<String> createUser(@RequestBody User user) {

		if (LOGGED_USER_ROLE.equals("ADMIN")) {
			user.setCreatedAt(LocalDateTime.now());
			userService.createUser(user, LOGGED_USER_ID);
			return ResponseEntity.status(HttpStatus.CREATED).body(Integer.toString(user.getId()));
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User Not Authorized");
		}
	}

	/**
	 * 
	 * @return list of users
	 */
	@GetMapping("/user")
	@ApiOperation(value = "Get all Users")
	private ResponseEntity<Iterable<User>> getAllUsers() {

		return ResponseEntity.status(HttpStatus.CREATED).body(userService.getAllUsers());
	}

	/**
	 * 
	 * @param userid user id
	 * @return user details
	 * @throws UserNotFoundException
	 */
	@GetMapping("/user/{userid}")
	@ApiOperation(value = "Get an user with id")
	private ResponseEntity<User> getUserById(@PathVariable("userid") int userid) throws UserNotFoundException {

		User user = userService.getUserById(userid);

		return ResponseEntity.status(HttpStatus.OK).body(user);
	}
	
	/**
	 * 
	 * @param action action
	 * @return list of audit records
	 */
	@SuppressWarnings("static-access")
	@GetMapping("/audit")
	@ApiOperation(value = "Gets list of audit records")
	public ResponseEntity<List<Audit>> getAuditRecords(@RequestParam(defaultValue = "POST") String action,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) Optional<LocalDate> fromDate,
			@RequestParam @DateTimeFormat(iso = ISO.DATE) Optional<LocalDate> toDate) {
		
		if(!fromDate.isPresent()) {
			fromDate = Optional.of(LocalDate.now().minusDays(1));
			System.out.println("fromDate--"+fromDate);
			toDate = Optional.of(LocalDate.now().plusDays(1));
			System.out.println("toDate--"+toDate);
		}

		List<Audit> list = userService.getAuditRecords(action, fromDate, toDate);

		return new ResponseEntity<List<Audit>>(list, HttpStatus.OK);
	}

	/**
	 * 
	 * @param userid user id
	 * @param user   user objects
	 * @return status
	 * @throws UserNotFoundException
	 */
	@PutMapping("/user/{userid}")
	@ApiOperation(value = "Updates user")
	private ResponseEntity<String> updateUser(@RequestBody User user, @PathVariable("userid") int userid)
			throws UserNotFoundException {

		if (LOGGED_USER_ROLE.equals("ADMIN")) {
			user.setId(userid);
			user.setModifiedAt(LocalDateTime.now());
			userService.updateUser(user, LOGGED_USER_ID);
			return ResponseEntity.status(HttpStatus.OK).body("Updated Successfully");
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User Not Authorized");
		}
	}

	/**
	 * 
	 * @param userid user id
	 * @return status
	 * @throws UserNotFoundException
	 */
	@DeleteMapping("/user/{userid}")
	@ApiOperation(value = "Deletes user")
	private ResponseEntity<String> deleteUSer(@PathVariable("userid") int userid) throws UserNotFoundException {

		if (LOGGED_USER_ROLE.equals("ADMIN")) {
			userService.deleteUser(userid, LOGGED_USER_ID);
			return ResponseEntity.status(HttpStatus.OK).body("Deleted Successfully");
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User Not Authorized");
		}
	}

}
